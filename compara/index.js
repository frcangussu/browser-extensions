var lance = [];
var agora = [];

document.onkeyup = function (e) {
    if (e.ctrlKey && e.shiftKey && e.which == 75) {
        compara();
    }
};

function compara() {
    lance = [];
    agora = [];
    try {
        // listFUTItem has-auction-data selected expired
        var listaFut = document.querySelector('li.listFUTItem');
        if (!listaFut) {
            return;
        }

        document.querySelectorAll('li.listFUTItem').forEach(item => {
            console.log('\n\n\n\t\t>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
            console.log('\t\t>>> item.querySelector(div.rowContent): ', item.querySelector('div.rowContent'));
            console.log('\t\t>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n');

            var valores = item.querySelectorAll('div.auctionValue');

            let valorLance = parseInt(valores[1].innerText.split('\n')[1].replace(/[^\d]+/g, ''));
            if (lance.indexOf(valorLance) == -1) lance.push(valorLance);

            let valorAgora = parseInt(valores[2].innerText.split('\n')[1].replace(/[^\d]+/g, ''));
            if (lance.indexOf(valorAgora) == -1) agora.push(valorAgora);
        });

        //ordenando arrays
        lance.sort((a, b) => a - b);
        agora.sort((a, b) => a - b);

        console.log(lance);
        console.log(agora);

        var realizarLance = 0;
        realizarLance += lance[1] / lance[0] > 1.4;
        var difLance = lance[1] - lance[0];

        var comprarAgora = 0;
        comprarAgora += agora[1] / agora[0] > 1.4;
        var difAgora = agora[1] - agora[0];

        if (comprarAgora) {
            alert('Pode COMPRAR AGORA por: ' + agora[0]);
        } else if (realizarLance) {
            alert('Pode dar o LANCE de: ' + lance[0]);
        } else {
            document.querySelector('body').style.border = 'solid 3000px #DDDDDD';
            window.setTimeout(() => {
                document.querySelector('body').style.border = '';
            }, 2000);
            // alert('nao vale a pena\n lance: ' + lance[0] + ' - ' + lance[1] + ' \n agora: ' + agora[0] + ' - ' + agora[1]);
        }
    } catch (error) {}
}
