function proximaPagina(){
    var url = new URL(document.location.href);
    var pag_lote = url.searchParams.get("pag_lote");
    
    if (pag_lote){
        var pagina = parseInt(pag_lote) + 1;
    } else {
        var pagina = 2;
    }

    document.location.href= document.location.origin+document.location.pathname+ "?pagination=true&pag_lote="+pagina;
}

document.onkeyup = function (e) {
    if (e.ctrlKey && e.shiftKey && e.which == 76) { // ctrl + shift + L
        limpar();
    } else if (e.ctrlKey && e.shiftKey && e.which == 75){ // ctrl + shift + K
        capturarLinks();
    }
};

function obterValor(doc){
    
    var documento = doc || document;

    var valores = documento.querySelectorAll(".value");
    var res = [];
    for (var i = 0; i < valores.length; i++){
        res[i] = valores[i].textContent
    }

    return res[0]+";"+res[2];
}

function capturar(doc,link,countLinks) {

    var resposta = {};

    var documento = doc || document;

    var linhas = documento.querySelector(".section-heading.text-center.marginbot20").nextElementSibling.querySelector("table").querySelectorAll("tr");
    linhas.forEach(function(item){
        // resposta += (item.children[1].textContent) + "\t";
        resposta[item.children[0].textContent] = item.children[1].textContent;
    });

    delete resposta["Acessórios:"];
    delete resposta["Observações:"];

    var res = "";
    for (var campo in resposta){
        res+= resposta[campo] + ";";
    }

    res += obterValor(documento);
    res += ";"+link;

    localStorage.setItem(link,res);
    
    countLinks.pop();
    if (countLinks.length===0){
        if (window.confirm("os dados foram coletados com sucesso\n\nDeseja ir para a próxima página?")){
            proximaPagina();
        }
        // alert('os dados foram coletados com sucesso')
    }

    console.log(localStorage);
}

function resultado() {

    var res = "";
    for (var i = 0; i < localStorage.length; i++) {
        res += localStorage.getItem(localStorage.key(i)) + "\n" ;
    }

    console.log(res);

    gerarArquivo("arquivo",res);

    localStorage.clear();
}

function gerarArquivo(nomeLista, conteudo) {

    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += conteudo;

    var encodedUri = encodeURI(csvContent);
    // window.open(encodedUri);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", nomeLista + ".csv");
    document.body.appendChild(link); // Required for FF

    link.click(); // This will download the data file named "my_data.csv".
}

function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
            let parser = new DOMParser();
            let cb = parser.parseFromString(xmlHttp.responseText, "text/html");
            callback(cb,theUrl);
        }
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

function getAllLinks(){
    var links = {};
    
    document.querySelectorAll("a")
    .forEach(function(e){
        if (e.href.includes("lote_info")){
            links[e.href] = e.href;
        }
    });

    return links;
}

function capturarLinks(){
    var links = getAllLinks();

    var countLinks = [];
    for (var link in links){
        countLinks.push(link);
        httpGetAsync(link, function(doc,url){
            capturar(doc,url, countLinks);
        } );
    }

}

function limpar(){
    localStorage.clear();
}