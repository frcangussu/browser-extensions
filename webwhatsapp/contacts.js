// > procurar contatos não cadastrados
// > criar arquivo CSV
// procurar mensagens "quero participar"
// procurar mensagens diferentes de "quero participar"
// procurar mensagens não lidas
// criar lista de transmissão

function gerarArquivo(nomeLista, conteudo) {

    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += conteudo;

    var encodedUri = encodeURI(csvContent);
    // window.open(encodedUri);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", nomeLista + ".csv");
    document.body.appendChild(link); // Required for FF

    link.click(); // This will download the data file named "my_data.csv".
}

// scroll down
function descer() {
    document.querySelector('div#pane-side').scrollTop += document.querySelector('div#pane-side').scrollWidth;
}

function irParaInicioDaTela() {
    document.querySelector('div#pane-side').scrollTop = 0;
}


var tamanhoLista = 250;
function getNumeroContato(contadorContatos) {
    var res = (contadorContatos % tamanhoLista) || contadorContatos / (contadorContatos / tamanhoLista);
    return ("000" + res).slice(-3);
}

function getNumeroLista(contadorContatos) {
    var res = parseInt((contadorContatos - 1) / tamanhoLista) + 1;
    return ("000" + res).slice(-3);
}

function init(nomeLista,nrLista,nrLead) {

    nomeLista = nomeLista || "LISTA";
    
    irParaInicioDaTela();
    
    var tempo = tempo || 20;
    
    var conteudo = "Nome;Telefone\r\n";
    
    nrLista = (nrLista) ? (parseInt(nrLista)-1)*250 : 0;
    nrLead  = (nrLead)  ?  parseInt(nrLead)-1 : 0;

    var contadorContatos = nrLista+nrLead;
    
    var refreshId = window.setInterval(function () {

        if (document.querySelector('div#pane-side').scrollTop < document.querySelector('div#pane-side').scrollTopMax) {

            document.querySelectorAll("._19RFN[title]:not([title=''])").forEach(function (e) {

                var telefoneContato = e.innerHTML;

                // checa se está no formato de telefone                    e   se ainda não foi incluido
                if (!conteudo.includes(telefoneContato)) {
                    
                    if (/\+\d{2}\s\d{2}\s\d{4,5}-?\d{4}/g.test(telefoneContato)) {

                        contadorContatos++;
    
                        var telefone = telefoneContato;
                        var numeroLista = getNumeroLista(contadorContatos);
                        var numeroContato = getNumeroContato(contadorContatos);
                        var nomeContato = nomeLista + " " + numeroLista + " - " + numeroContato;

                        conteudo += nomeContato + ";" + telefone + "\r\n";
                    } 
                }

            });

            descer();

            var percent = parseInt(document.querySelector('div#pane-side').scrollTop * 100 / document.querySelector('div#pane-side').scrollTopMax);

            // console.log(percent+"%");

        } else {
            window.clearInterval(refreshId);
            gerarArquivo(nomeLista, conteudo);
            console.log(">>>> contatos: ", contadorContatos);
        }
    }, tempo);
}