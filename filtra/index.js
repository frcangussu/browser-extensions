document.onkeyup = function (e) {
    if (e.ctrlKey && e.shiftKey && e.which == 76) {
        filtrar();
    }
};

function limpar(){
    delete sessionStorage.rating;
}

function filtrar() {
    // limparImagens();
    try {
        var listaFut = document.querySelector('li.listFUTItem');

        if (!listaFut) {
            // alert('Você não está no endereço correto');
            return;
        }

        var storage = parseInt(sessionStorage.getItem('rating'));
        var rating = storage || parseInt(prompt('Informe o rating mínimo:', '83'));
        sessionStorage.setItem('rating', rating);

        // let qualidade = window.prompt('Informe o a qualidade mínima', '84');

        document.querySelectorAll('li.listFUTItem').forEach(item => {
            let rat = item.querySelector('div.rating');
            if (parseInt(rat.innerText) < rating) {
                // item.innerHTML = "";
                item.parentNode.removeChild(item);
            }
        });
    } catch (error) {}
}

function limparImagens() {
    document.querySelectorAll('img').forEach(item => {
        item.src = '';
    });
}

function proxima() {
    // btn-flat pagination next
    // document.querySelector('a.next').addEventListener('click', function() {
    document.querySelector('a.next').click();
}

function teste() {
    alert('teste');
}
